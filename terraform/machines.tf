resource "proxmox_virtual_environment_vm" "debian_vm" {
  name        = var.vm_name
  description = var.vm_description
  vm_id       = var.vm_id
  node_name   = var.node_name
  tags        = ["debian", "vm"]

  initialization {
    ip_config {
      ipv4 {
        address = var.vm_ip_address
        gateway = var.vm_netgateway
      }
    }
  }

  clone {
    vm_id        = var.clone_vmid
    full         = true
    datastore_id = var.datastore_id
  }

  agent {
    enabled = true
  }

  startup {
    order      = "3"
    up_delay   = "60"
    down_delay = "60"
  }
}

#---------------------------------------------------------------------------

provider "proxmox" {
  endpoint  = var.proxmox_url
  api_token = var.api_token
  insecure  = true
}