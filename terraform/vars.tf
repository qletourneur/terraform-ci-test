variable "proxmox_url" {
  type    = string
  default = "https://172.16.10.253:8006/"
}

variable "api_token" {
  type      = string
  default   = "qletourneur@pve!terraform=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
  sensitive = true
}

variable "node_name" {
  type    = string
  default = "pve"
}

variable "vm_name" {
  type    = string
  default = "debvm01"
}

variable "vm_description" {
  type    = string
  default = "Debian VM N°01"
}

variable "vm_id" {
  type    = string
  default = "200"
}

variable "vm_ip_address" {
  type    = string
  default = "172.16.10.101/24"
}

variable "vm_netgateway" {
  type    = string
  default = "172.16.10.1"
}

variable "clone_vmid" {
  type    = string
  default = "9999"
}

variable "datastore_id" {
  type    = string
  default = "local-lvm"
}